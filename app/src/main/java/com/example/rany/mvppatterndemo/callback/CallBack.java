package com.example.rany.mvppatterndemo.callback;

public interface CallBack {

    void onSuccess();
    void onFailure();

}
