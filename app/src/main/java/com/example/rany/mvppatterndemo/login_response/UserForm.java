package com.example.rany.mvppatterndemo.login_response;

import com.google.gson.annotations.SerializedName;

public class UserForm {
    @SerializedName("EMAIL")
    private String email;
    @SerializedName("PASSWORD")
    private String password;

    public UserForm(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
