package com.example.rany.mvppatterndemo.login_response;

import com.google.gson.annotations.SerializedName;

public class UserResponse {
    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    private User usersignup;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUsersignup() {
        return usersignup;
    }

    public void setUsersignup(User usersignup) {
        this.usersignup = usersignup;
    }
}
