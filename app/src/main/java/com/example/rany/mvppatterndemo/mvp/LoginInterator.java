package com.example.rany.mvppatterndemo.mvp;

import android.util.Log;

import com.example.rany.mvppatterndemo.callback.CallBack;
import com.example.rany.mvppatterndemo.login_response.UserForm;
import com.example.rany.mvppatterndemo.login_response.UserResponse;
import com.example.rany.mvppatterndemo.service.LoginService;
import com.example.rany.mvppatterndemo.service.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginInterator implements LoginMVP.LoginInterator {

    private static final String TAG = "ooooo";
//    private String email = "admin@gmail.com";
//    private String password = "admin123";

    private LoginService loginService =
            ServiceGenerator.createService(LoginService.class);

    @Override
    public void onLogin(final String email, final String password, final CallBack callBack) {
        loginService.userLogin(new UserForm(email, password))
                .enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call,
                                           Response<UserResponse> response) {
                        if(response.isSuccessful()){
                            if(response.body().getUsersignup() != null){
                                callBack.onSuccess();
                            }
                            else
                                callBack.onFailure();
                        }
                    }
                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        Log.e(TAG, "onFailure: "+ t.getMessage() );
                    }
                });

    }

}
