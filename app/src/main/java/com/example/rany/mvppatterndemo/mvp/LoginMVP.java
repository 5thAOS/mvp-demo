package com.example.rany.mvppatterndemo.mvp;

import com.example.rany.mvppatterndemo.callback.CallBack;

public interface LoginMVP {

    interface LoginView{
        void showLoading();
        void hideLoading();
        void onLoginSuccess();
        void onLoginFailure();
    }

    interface LoginPresentor{
        void onLogin(String email, String password);
    }

    interface LoginInterator{
        void onLogin(String email, String password, CallBack callBack);
    }

}
