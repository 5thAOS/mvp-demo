package com.example.rany.mvppatterndemo.mvp;

import com.example.rany.mvppatterndemo.callback.CallBack;

public class LoginPresentor implements LoginMVP.LoginPresentor {

    private LoginMVP.LoginView view;
    private LoginMVP.LoginInterator interator;

    public LoginPresentor(LoginMVP.LoginView view) {
        this.view = view;
        interator = new LoginInterator();
    }

    @Override
    public void onLogin(String email, String password) {
        interator.onLogin(email, password, new CallBack() {
            @Override
            public void onSuccess() {
                view.onLoginSuccess();
            }

            @Override
            public void onFailure() {
                view.onLoginFailure();
            }
        });
    }
}
