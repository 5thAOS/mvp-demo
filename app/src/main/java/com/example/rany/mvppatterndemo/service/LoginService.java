package com.example.rany.mvppatterndemo.service;

import com.example.rany.mvppatterndemo.login_response.UserForm;
import com.example.rany.mvppatterndemo.login_response.UserResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginService {

    @POST("/v1/api/authentication")
    Call<UserResponse> userLogin(@Body UserForm userForm);

}