package com.example.rany.mvppatterndemo.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.rany.mvppatterndemo.R;
import com.example.rany.mvppatterndemo.mvp.LoginMVP;
import com.example.rany.mvppatterndemo.mvp.LoginPresentor;

public class LoginScreen extends AppCompatActivity implements LoginMVP.LoginView {

    private static final String TAG = "ooooo";
    private Button btnLogin;
    private EditText email, password;
    private LoginMVP.LoginPresentor presentor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presentor = new LoginPresentor(this);

        initView();
        iniEvent();

    }

    private void iniEvent() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presentor.onLogin(email.getText().toString(),
                        password.getText().toString());
            }
        });
    }

    private void initView() {
        btnLogin = findViewById(R.id.btnLogin);
        email = findViewById(R.id.tvEmail);
        password = findViewById(R.id.tvPassword);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onLoginSuccess() {
        Log.e(TAG, "onLoginSuccess: " );
    }

    @Override
    public void onLoginFailure() {
        Log.e(TAG, "onLoginFailure: " );
    }
}
